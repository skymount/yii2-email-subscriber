Email subscriber system
=======================
CMS plugin

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist skymount/yii2-email-subscriber "*"
```

or add

```
"skymount/yii2-email-subscriber": "*"
```

to the require section of your `composer.json` file.

Proccess migration
```
./yii migrate/up -p @vendor/skymount/yii2-email-subscriber/migrations
```

Usage
-----

Once the extension is installed, simply use it in your code by  :

```PHP
    'modules' => [
        ...
        'subscriber' => [
            'class' => 'skymount\messaging\Module',
        ],
    ],
```

```TWIG
<div class="container">
    {{ use('skymount/messaging/EmailSubscriber') }}
    {{ email_subscriber_widget() }}
</div>
```
