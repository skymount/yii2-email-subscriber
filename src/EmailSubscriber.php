<?php

namespace skymount\messaging;

use skymount\messaging\models\SubscribeForm;

class EmailSubscriber extends \yii\base\Widget
{
    public $action = ['/subscriber/add-email'];
    public $agreementLink = ['/agreement'];

    public function run()
    {
        echo $this->view->renderFile(__DIR__ . '/views/_widgets/email-subscriber.php', [
            'widgetId' => $this->id,
            'model' => new SubscribeForm(),
            'action' => $this->action,
            'agreementLink' => $this->agreementLink,
        ]);
    }
}
