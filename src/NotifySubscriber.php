<?php

namespace skymount\messaging;

use Yii;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use skymount\messaging\models\Subscriber;

class NotifySubscriber
{
    public function notify(string $scope, array $dataConfig)
    {
        set_time_limit(600);

        $postQuery = $dataConfig['query']();

        $countQuery = clone $postQuery;
        if ($countQuery->count() == 0) {
            return;
        }

        $message = $this->generateMessage($postQuery);

        $subscribersQuery = Subscriber::find()
            ->where('JSON_EXTRACT(subscriptions, "$.' . $scope . '") = TRUE');

        foreach ($subscribersQuery->batch(5) as $subscribers) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $this->notifyByEmail($subscribers, $message, $scope);
                $transaction->commit();
            } catch (\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
    }

    private function notifyByEmail(array $subscribers, $posts, $scope)
    {
        foreach ($subscribers as $subscriber) {
            Yii::$app->mailer
                ->compose(
                    ['text' => '@vendor/skymount/yii2-email-subscriber/src/mail/subscriber-' . $scope . '-text.php'],
                    [
                        'posts' => $posts,
                        'subscriber' => $subscriber,
                    ]
                )
                ->setSubject('Сообщение с сайта ' . Yii::$app->params['appName'] . '. Актуальное на ' . date('d.m.y'))
                ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderEmail']])
                ->setTo($subscriber->email)
                ->send();

            $subscriber->last_send_at = time();
            $subscriber->save();

            echo "{$subscriber['email']}\n";
        }
    }

    private function generateMessage($query)
    {
        return $query
            ->select([
                'id',
                'slug',
                'title',
                'created_at',
            ])
            ->orderBy('created_at ASC')
            ->asArray()
            ->all();
    }
}
