<?php

use \yii\helpers\Url;
use \yii\bootstrap4\ActiveForm;

?>
<div class="email-subscriber__widget">
    <?php $form = ActiveForm::begin([
        'action' => $action,
        'method' => 'post',
        'options' => ['class' => 'email-subscriber__form form'],
        'successCssClass' => false,
    ]) ?>

        <div class="email-subscriber__title">Подпишитесь на наши новости и мероприятия</div>

        <?= $form->field($model, 'email')
            ->textInput([
                'placeholder' => 'Введите свой адрес эл.почты для получения писем',
                'required' => true,
                'class' => 'email-subscriber__email',
                'onclick' => "$('#{$widgetId}-reCaptcha').fadeIn();",
            ])->label(false) ?>

        <div id="<?= $widgetId ?>-reCaptcha" style="display: none;">
            <?= $form->field($model, 'reCaptcha') ->widget('\\himiklab\\yii2\\recaptcha\\ReCaptcha2', ['theme' => 'light']) ->label(false) ?>
        </div>

        <?= $form->field($model, 'agree', ['options' => ['class' => 'email-subscriber__notice']])
            ->checkbox()
            ->label('Я согласен(а) на <a href="' . Url::to($agreementLink) . '" target="_blank">обработку моих персональных данных</a>') ?>

        <button type="submit" class="btn email-subscriber__submit">Подписаться</button>

    <?php ActiveForm::end() ?>
</div>
