<?php

?>

<div class="subscriptions">
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'email',
            [
                'label' => 'Добавлено',
                'attribute' => 'created_at',
                'value' => function ($model) {
                    return date('d.m.Y H:i', $model->created_at);
                },
            ],
            [
                'label' => 'Отправлено',
                'attribute' => 'last_send_at',
                'value' => function ($model) {
                    return date('d.m.Y H:i', $model->last_send_at);
                },
            ],
        ],
    ]) ?>
</div>

<style>
    .subscriptions {
        max-width: 800px;
        margin: 40px auto;
    }
</style>