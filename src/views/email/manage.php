<?php

use \yii\helpers\Url;
use \yii\helpers\Html;

$state = array_map(function ($el) {
    return $el ? 'Вкл.' : 'Выкл.';
}, (array) $subscriber->subscriptions);

?>
<div class="subscriber-manage d-flex flex-column align-items-center justify-content-center" style="padding: 200px 0;">
    <div class="subscriber-manage__title font-weight-bolder mb-4">
        Ваш почтовый адрес: <?= $subscriber->email ?>
    </div>

    <div class="subscriber-manage__notice card">
        <div class="card-header">Изменить настройки подписки</div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <?= Html::a('Получение новостей', ['switch-subscription', 'uuid' => $subscriber->uuid, 'type' => 'news'], ['data-method' => 'post']) ?> (<?= $state['news'] ?>)
            </li>
            <li class="list-group-item">
                <?= Html::a('Получение анонса мероприятий', ['switch-subscription', 'uuid' => $subscriber->uuid, 'type' => 'event'], ['data-method' => 'post']) ?> (<?= $state['event'] ?>)
            </li>
        </ul>
    </div>

    <a class="my-5 text-muted" href="<?= Url::to(['delete', 'uuid' => $subscriber->uuid]) ?>" data-method="post" data-confirm="Действительно удалить?">Удалить адрес из списка рассылок.</a>

    <a class="mt-5 text-muted" href="/">Перейти на сайт</a>
</div>