<?php

namespace skymount\messaging\models;

use Yii;
use yii\base\Model;
use skymount\messaging\models\Subscriber;

/**
 * SubscribeForm is the model behind the Contact form.
 */
class SubscribeForm extends Model
{
    public $email;
    public $reCaptcha;
    public $agree;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'string', 'max' => 120],
            [['email'], 'required'],
            [['email'], 'email'],
            [['agree'], 'required', 'requiredValue' => 1, 'message' => 'Для продолжения необходимо дать согласие на обработку персональных данных.'],
            [['reCaptcha'], \himiklab\yii2\recaptcha\ReCaptchaValidator2::className(), 'uncheckedMessage' => 'Подтвердите, что вы не робот.'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Эл.почта',
        ];
    }

    public function registerSubscriber()
    {
        return \skymount\messaging\models\Subscriber::createSubscriber($this->email);
    }

    public function notify(Subscriber $subscriber)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['text' => '/../mail/subscriber-hello-text.php'],
                ['subscriber' => $subscriber]
            )
            ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderEmail']])
            ->setTo($subscriber->email)
            ->setSubject('Сообщение с сайта ' . (@Yii::$app->params['appName'] ?: Yii::$app->name))
            ->send();
    }
}
