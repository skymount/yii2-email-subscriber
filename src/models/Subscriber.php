<?php

namespace skymount\messaging\models;

use Yii;

/**
 * This is the model class for table "skm_subscriber".
 *
 * @property int $id
 * @property string $email
 * @property string $uuid
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $verified_at
 * @property int|null $last_send_at
 * @property string|null $subscriptions
 * @property string|null $verification_token
 */
class Subscriber extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'skm_subscriber';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'uuid'], 'required'],
            [['subscriptions'], 'safe'],
            [['email'], 'string', 'max' => 120],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('skymount-subscriber', 'ID'),
            'email' => Yii::t('skymount-subscriber', 'Email'),
            'uuid' => Yii::t('skymount-subscriber', 'UUID'),
            'created_at' => Yii::t('skymount-subscriber', 'Created At'),
            'updated_at' => Yii::t('skymount-subscriber', 'Updated At'),
            'last_send_at' => Yii::t('skymount-subscriber', 'Last Send At'),
            'subscriptions' => Yii::t('skymount-subscriber', 'Subscriptions'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            \yii\behaviors\TimestampBehavior::className(),
        ];
    }

    private static $defaultSubscriptions = [
        'news' => true,
        'event' => true,
    ];

    public static function createSubscriber(string $email)
    {
        $model = Subscriber::findOne(['email' => $email]);

        if (empty($model)) {
            try {
                $model = new Subscriber([
                    'email' => $email,
                    'uuid' => YIi::$app->uuid->generate('v5', \skymount\messaging\SubscriberSchema::NS_EMAIL, $email),
                    'subscriptions' => static::$defaultSubscriptions,
                    'verification_token' => Yii::$app->security->generateRandomString() . '_' . time(),
                ]);
                if (!$model->save()) {
                    throw new \Exception('Не удалось создать подписчика.');
                }
                return $model;
            } catch (\Exception $e) {
                throw $e;
            }
        }

        return null;
    }

    public function switchSubscription($type)
    {
        try {
            $subscriptions = $this->subscriptions;

            switch ($type) {
                case 'news':
                    $subscriptions['news'] = !$subscriptions['news'];
                    break;
                
                case 'event':
                    $subscriptions['event'] = !$subscriptions['event'];
                    break;
            }

            $this->subscriptions = $subscriptions;

            if (!$this->save()) {
                throw new \Exception('Не удалось обновить конфигурацию подписчика.');
            }
        } catch (\Exception $e) {
            throw $e;
        }
   }
}
