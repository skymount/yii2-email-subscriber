<?php
/* @var $this yii\web\View */
?>
Здравствуйте.

В этом письме новости нашего сайта за прошедшую неделю.

<?php foreach ($posts as $key => $post): ?>
<?= sprintf("%s) %s\n%s\n\n", $key + 1, $post['title'], "https://znanie43.ru/news/{$post['id']}-{$post['slug']}") ?>
<?php endforeach; ?>

---
Если это письмо попало к Вам по ошибке, либо по иной причине - отменить подписку можно по этой ссылке:
https://znanie43.ru/subscriber/manage/<?= $subscriber->uuid ?>
