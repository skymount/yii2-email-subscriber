<?php
/* @var $this yii\web\View */
?>
Здравствуйте.

В этом письме события и мероприятия добавленные на сайт сегодня.

<?php foreach ($posts as $key => $post): ?>
<?= sprintf("%s) %s\n%s\n\n", $key + 1, $post['title'], "https://znanie43.ru/event/{$post['id']}-{$post['slug']}") ?>
<?php endforeach; ?>

---
Если это письмо попало к Вам по ошибке, либо по иной причине - отменить подписку можно по этой ссылке:
https://znanie43.ru/subscriber/manage/<?= $subscriber->uuid ?>
