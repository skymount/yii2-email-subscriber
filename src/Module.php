<?php

namespace skymount\messaging;

use Yii;

/**
 * SkyAdmin module definition class
 */
class Module extends \yii\base\Module implements \yii\base\BootstrapInterface
{
    public $senderEmail;

    /**
     * {@inheritdoc}
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules([
            new \yii\web\GroupUrlRule([
                'prefix' => 'subscriber',
                'rules' => [
                    'add-email'           => 'email/add',
                    'delete-email/<uuid>' => 'email/delete',
                    'manage/<uuid>'       => 'email/manage',
                    'subscription/<uuid>' => 'email/switch-subscription',
                    'subscriptions'       => 'email/subscriptions',
                ],
            ]),
        ]);

        $app->i18n->translations['skymount-subscriber'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => __DIR__ . '/messages',
        ];
    }
}
