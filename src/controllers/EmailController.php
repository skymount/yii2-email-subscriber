<?php

namespace skymount\messaging\controllers;

use Yii;
use yii\helpers\Url;
use yii\filters\VerbFilter;
use skymount\messaging\models\Subscriber;
use skymount\messaging\models\SubscribeForm;

/**
 * EmailController
 */
class EmailController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'add' => ['POST'],
                    'delete' => ['POST'],
                    'switch-subscription' => ['POST'],
                ],
            ],
        ];
    }

    public function actionAdd()
    {
        $model = new SubscribeForm;

        if ($model->load(Yii::$app->request->post())) {
            $subscriber = $model->registerSubscriber();
            if ($subscriber instanceof Subscriber) {
                $model->notify($subscriber);
                Yii::$app->session->setFlash('success', 'Ваш адрес успешно зарегистрирован для рассылки обновлений.');
            } elseif ($subscriber === false) {
                Yii::$app->session->setFlash('error', 'Не удалось зарегистрировать ваш адрес. Повторите попытку через некоторое время.');
            } else {
                Yii::$app->session->setFlash('warning', 'Этот адрес ранее уже был добавлен в рассылку.');
            }
        }

        return $this->redirect(Yii::$app->request->referrer ?: '/');
    }

    public function actionManage(string $uuid = '')
    {
        if (empty($uuid) || !($subscriber = Subscriber::findOne(['uuid' => $uuid]))) {
            throw new \yii\web\NotFoundHttpException();
        }

        return $this->render('manage', [
            'subscriber' => $subscriber,
        ]);
    }

    public function actionSwitchSubscription(string $uuid = '', string $type = '')
    {
        if (empty($type) || empty($uuid) || !($subscriber = Subscriber::findOne(['uuid' => $uuid]))) {
            throw new \yii\web\NotFoundHttpException();
        }

        $subscriber->switchSubscription($type);

        return $this->redirect(['/subscriber/email/manage', 'uuid' => $uuid]);
    }

    public function actionDelete(string $uuid = '')
    {
        if (empty($uuid) || !($subscriber = Subscriber::findOne(['uuid' => $uuid]))) {
            throw new \yii\web\NotFoundHttpException();
        }

        $subscriber->delete();
        Yii::$app->session->setFlash('success', 'Адрес эл.почты удален из списка рассылок.');

        return $this->redirect('/');
    }

    public function actionSubscriptions()
    {
        if (\Yii::$app->user->isGuest) {
            throw new \yii\web\NotFoundHttpException();
        }

        return $this->render('subscriptions', [
            'dataProvider' => new \yii\data\ActiveDataProvider([
                'query' => Subscriber::find(),
                'pagination' => [
                    'pageSize' => 30,
                ],
            ]),
        ]);
    }
}