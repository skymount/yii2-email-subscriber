<?php

namespace skymount\messaging\controllers;

use yii\console\ExitCode;
use yii\helpers\ArrayHelper;
use skymount\messaging\NotifySubscriber;

/**
 * ConsoleController
 */
class ConsoleController extends \yii\console\Controller
{
    public $selectedScope;

    public $scopes;

    public function options($actionID)
    {
        return [
            'selectedScope',
        ];
    }

    public function optionAliases()
    {
        return [
            's' => 'selectedScope',
        ];
    }

    public function actionNotify()
    {
        if (!is_array($this->scopes) || empty($this->scopes)) {
            throw new \yii\base\InvalidConfigException('Параметр Scopes не задан.');
        }

        $dataConfig = ArrayHelper::getValue($this->scopes, $this->selectedScope);

        if (empty($dataConfig) || !isset($dataConfig['query'])) {
            throw new \yii\base\InvalidConfigException('Параметр selectedScope задан, но не найден источник данных для него.');
        }

        if ((new NotifySubscriber)->notify($this->selectedScope, $dataConfig) !== false) {
            return ExitCode::OK;
        } else {
            return ExitCode::UNSPECIFIED_ERROR;
        }
    }
}
