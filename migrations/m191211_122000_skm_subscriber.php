<?php

use yii\db\Migration;

/**
 * Class m191211_122000_skm_subscriber
 */
class m191211_122000_skm_subscriber extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';

        $this->createTable('{{%skm_subscriber}}', [
            'id'                 => $this->primaryKey()->unsigned(),
            'email'              => $this->string(120)->unique()->notNull(),
            'uuid'               => $this->string(36)->unique()->notNull(),
            'created_at'         => $this->integer(),
            'updated_at'         => $this->integer(),
            'verified_at'        => $this->integer(),
            'last_send_at'       => $this->integer(),
            'subscriptions'      => $this->json(),
            'verification_token' => $this->string()->defaultValue(null),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%skm_subscriber}}');
    }
}